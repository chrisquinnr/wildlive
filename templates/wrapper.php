<html>
  <head>
    <script src='https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Sigmar+One' rel='stylesheet' type='text/css'>
    <link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.css' rel='stylesheet' />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <link type="image/ico" href="/assets/images/favicon.ico" rel="icon" />
    <title>WildLive</title>
  </head>
  <body>
    <header>
      <h1>WildLive</h1>
      
      <nav>
        <ul>
          <li><a href="/">Home</a></li>
          <li><a href="/profile">My Profile</a></li>
          <li><a href="events">Events</a></li>
          <li><a href="/about">About</a></li>
          <li><a href="/help">Help</a></li>
        </ul>
      </nav>
    </header>
    <iframe class="aside" src="/login"></iframe>
    <div id="map" class="main-map"></div>

    <script src="/assets/vendor/jQuery/dist/jquery.min.js"></script>
    <script>

    L.mapbox.accessToken = 'pk.eyJ1IjoiY2hyaXNxdWlubnIiLCJhIjoiVUNSUHViVSJ9.kVqdPg8zJZi97e5du9A4FA';
    var map = L.mapbox.map('map', 'chrisquinnr.l563l41d'),
        previousPoints = [];

    function addToMap(data, options){
      var myLayer = L.mapbox.featureLayer().addTo(map);
      myLayer.on('layeradd', function(e) {
        var marker = e.layer,
            feature = marker.feature;
        marker.setIcon(L.icon(feature.properties.icon));

        if(typeof options.afterPoint != 'undefined'){
          options.afterPoint(e, map);
        }

        // map.setView(e.target._geojson[0].geometry.coordinates.reverse(), 16);
      });
      myLayer.setGeoJSON([data]);
    }

    function STO(points, options){
      setTimeout(function(){

        if(points.length > 0){
          addToMap(points.pop(), options);
          STO(points, options);
        } else {
          getData(options);
        }

      }, 200);
    }


    function getData(options){

      $.getJSON('/api', function(points){

        points.sort(function(a,b){

          var aD = new Date(a.properties.dateAdded),
              bD = new Date(b.properties.dateAdded);

          return aD.getTime() > bD.getTime();

        });

        if(points.length > previousPoints.length){
          var diff = points.length-previousPoints.length,
              newPoints = points.slice(points.length-diff, points.length);

          // console.log(newPoints);

          STO(newPoints, options);

        } else {
          setTimeout(function(){
            getData({
              afterPoint: function(e, map){
                map.setView(e.target._geojson[0].geometry.coordinates.reverse(), 16);
              }
            })
          }, 200);
        }

        previousPoints = points;

      });

    }

    getData({});



  	</script>
  </body>
</html>
