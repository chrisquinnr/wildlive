<html>
  <head>
    <script src='https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Sigmar+One' rel='stylesheet' type='text/css'>
    <link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.css' rel='stylesheet' />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <title>WildLive</title>
    <script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "c3b2fc1b-5909-4d3f-af32-11abcd3b5da5", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
  </head>
  <body>
    <header>
      <h1>WildLive</h1>
      
      <nav>
        <ul>
          <li><a href="/">Home</a></li>
          <li><a href="/profile">My Profile</a></li>
          <li><a href="events">Events</a></li>
          <li><a href="/about">About</a></li>
          <li><a href="/help">Help</a></li>
        </ul>
      </nav>
    </header>
    
    <div role="main">
    	<h1>Upcoming Events</h1>
    	<div class="row">
    		Get involved in Citizen Science! 
    	</div>
    	<div class="row left">
    		<table>
    			<tr style="border-top:1px dotted black">
    				<td width="20%">
	    				<img width="200" src='/assets/images/orgs/bnhc.png' style="float:left;padding:20;">
	    			</td>
	    			<td width="40%">
	    				<strong>BioBlitz // BS1 </strong><br/>
	    				<em>All year round</em><br/>
			    		Take part in one of the many BioBlitz events running all over the UK. Every event is different and there are so many amazing spaces to discover and explore. Grab you binoculars and bug pots and find a BioBlitz event near you!<br/>
			    	</td>
			    	<td width="40%">
			    		<a class="button" href="/submit">Take part!</a>
			    	</td>
			    </tr>
    			<tr style="border-top:1px dotted black">
    				<td width="20%">
	    				<img width="200" src='/assets/images/orgs/bbct.png' style="float:left;padding:20;">
	    			</td>
	    			<td width="40%">
	    				<strong>Bumblebee Conservation Trust </strong><br/>
	    				<em>All year round</em><br/>
			    		BBCT staff and volunteers deliver events up and down the country throughout the year. These include bumblebee themed exhibits at public events, talks and guided bumblebee walks for nature and gardening groups, plus bee identification training courses. Check out our events calendar for upcoming events in your area or, if you would like to invite one of our representatives to deliver an event in your area, contact us at enquiries@bumblebeeconservation.org.
			    	</td>
			    	<td width="40%">
			    		<a class="button" href="/submit">Take part!</a>
			    	</td>
			    </tr>
    			<tr style="border-top:1px dotted black">
    				<td width="20%">
	    				<img width="200" src='/assets/images/orgs/bbc.jpg' style="float:left;padding:20;">
	    			</td>
	    			<td width="40%">
	    				<strong>Springwatch</strong><br/>
	    				<em>Feb - May</em><br/>
			    		Ladybirds are important to local ecosystems and provide natural pest control to gardeners. But they face threats too.
Do One Thing for nature - spend some time looking for ladybirds and join the UK Ladybird Survey run by the Centre for Ecology & Hydrology (CEH). 
			    	</td>
			    	<td width="40%">
			    		<a class="button" href="/submit">Take part!</a>
			    	</td>
			    </tr>
	        </table>
    		
    	</div>
    	<hr/>
    	<div class="row">
    	</div>
    </div>
    
    
    </body>
    
</html>