<html>
  <head>
    <script src='https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Sigmar+One' rel='stylesheet' type='text/css'>
    <link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.css' rel='stylesheet' />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <title>WildLive</title>
  </head>
  <body>
  	<div role="main">
	  	<p>&nbsp;</p>
	    <p><strong>Thanks for submitting a record!</strong></p>
		<p>You've helped record valuable distribution data.<br/>
			Using your report, we can<br/>
			- can track changes in range or detect declines as they are happening, <br/>
			- advise conservation plans that mitigate threats and help vulnerable populations,<br/>
			- create a rich and sustainable archive of data for future generations.<br/>
	
		</p>
		<div style="display: block;" class="submit-a-sighting-cta">
	        <a class="button" href="/submit">Send another report!</a>
	      </div>
	 </div>
  </body>
</html>