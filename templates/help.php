<html>
  <head>
    <script src='https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Sigmar+One' rel='stylesheet' type='text/css'>
    <link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.css' rel='stylesheet' />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <title>WildLive</title>
  </head>
  <body>
<div role="main">
    <h2>Help</h2>
    <em><a href='/submit'>Back to the form</a></em>
    <ul>
	    <li><a href='#deer'>Deer</a></li>
		<li><a href='#rabbit'>Rabbit</a></li>
		<li><a href='#hedgehog'>Hedgehog</a></li>
		<li><a href='#fox'>Fox</a></li>
		<li>Badger</a></li>
		<li>Flowers</a></li>
		<li>Trees</a></li>
	</ul>
	
	<a name='deer'></a>
	<div class="row">
		<p>
			<img width="300" src='http://thumb9.shutterstock.com/display_pic_with_logo/2264336/193179122/stock-photo-roe-deer-193179122.jpg'>
			Deer (Roe) <a href='#top'><small>Back to top</small></a>
		</p>
		<p>
			"The Roe Deer is quite a small deer, with a body length of 95 - 135 centimetres (3.1 - 4.4 feet), a shoulder height of 65 - 75 centimetres (2.1 - 2.5 feet) and weighing 15 - 30 kilograms (33 - 66 pounds).
			The Roe Deer has rather short, erect antlers and a reddish body with a grey face. Its hide is golden red in summer, darkening to brown or even black in winter, with lighter undersides and a white rump patch.
			The Roe Deers tail is very short (2 - 3 centimetres (0.8 - 1.2 inches) and barely visible. Only the males have antlers, which are lost during winter, but which re-grow in time for the mating season. The first and second set of antlers are unbranched and short (5 - 12 centimetres (2 - 4.7 inches), while older bucks in good conditions develop antlers up to 20 - 25 centimetres (8 - 10 inches) long with 2 or 3, rarely even four, points."
		</p>
	</div>
	<div class="row">
		<p>
			<img width="300" src='http://thumb7.shutterstock.com/display_pic_with_logo/499885/178418375/stock-photo-three-young-fallow-deer-near-woodland-at-bradgate-park-in-leicestershire-england-178418375.jpgFallow'>
			Deer (Fallow) <a href='#top'><small>Back to top</small></a>
		</p>
		<p>
			"Male fallow deer (bucks) have 'palmate' antlers - a wider and flatter spread with less distinct tines than the red deer, these are broad and shaped like a shovel. Female fallow deer (does) do not have antlers. Young fallow deer are called 'fawns'.
			Bucks measure around 140 - 160 centimetres in length, 90 - 100 centimetres in shoulder height and weigh around 60 - 85 kilograms. Does measure 130 - 150 centimetres in length, have a shoulder height of 75 - 85 centimetres and weigh 30 - 50 kilograms. Fawns are born in springtime and measure around 30 centimetres in height and weigh around 4.5 kilograms.
			Fallow deer are very variable in colour, with four main varieties, 'common', 'menil', 'melanistic' and 'albinistic'. The common form has a bright chestnut coat with white mottles that are most pronounced in summer with a much darker, drab grey-brown coat in the winter. The albinistic is the lightest coloured, almost white. Common and menil are darker and melanistic is very dark, even black.
			Most herds consist of the common form but have menil form and melanistic form animals amongst them. Fallow deer have yellow-white undersides, white spots and a black line that runs along the back to the tip of the tail. The spots become less prominent or disappear completely in winter."
		</p>
	</div>
	<a name='rabbit'></a>
	<div class="row">
		<p>
			<img width="300" src='http://thumb7.shutterstock.com/display_pic_with_logo/456427/187170455/stock-photo-rabbit-oryctolagus-cuniculus-sitting-in-vegitation-187170455.jpg'>
			Rabbit (Wild) <a href='#top'><small>Back to top</small></a>
		</p>
		<p>
			"The male is called a buck and the female a doe. Rabbits generally measure 40 - 45 centimetres in length and have ears that measure 8.5 centimetres long. They have compact bodies with long, powerful hind legs.
			A Rabbits fur is generally long and soft and is grey/brown in colour and they have white underparts and a short tail. Their long ears of rabbits are most likely an adaptation for detecting predators. Wild rabbits are rather uniform in body proportions and stance."
		</p>
	</div>
	<a name='hedgehog'></a>
		<div class="row">
		<p>
			<img width="300" src='http://thumb1.shutterstock.com/display_pic_with_logo/81009/118149634/stock-photo-hedgehog-118149634.jpg'>
			Hedgehog (Wild) <a href='#top'><small>Back to top</small></a>
		</p>
		<p>
			"Hedgehogs are easily recognized by their spines, which are hollow hairs made stiff with keratin. Their spines are not poisonous or barbed and unlike the quills of a porcupine, cannot easily be removed from the animal. However, spines normally come out when a hedgehog sheds baby spines and replaces them with adult spines. This is called 'quilling'.
			When under extreme stress or during sickness, a hedgehog can also lose its spines. Their under-parts are covered by coarse grey-brown fur, forming long skirts along flanks. A hedgehog measures around 23 centimetres in length and has a 4 centimetre tail. They can weigh up to 2 kilograms. The Hedgehog has a powerful forefoot and claws for digging.
			Hedgehogs have 5 toes on their front paws with short nails. However, on their back paws they have 4 toes with long, constantly growing nails. They have these characteristics because hedgehogs burrow. They can also climb, swim and can sprint a surprisingly fast 6 miles per hour."
		</p>
	</div>
	<a name='fox'></a>
		<div class="row">
		<p>
			<img width="300" title='Ring-ding-ding-ding-dingeringeding! Gering-ding-ding-ding-dingeringeding! Gering-ding-ding-ding-dingeringeding!' src='http://thumb9.shutterstock.com/display_pic_with_logo/993533/194313716/stock-photo-red-fox-standing-on-log-looking-at-viewer-through-thick-foliage-red-fox-red-fox-194313716.jpg'>
			Fox (Wild / Musical) <a href='#top'><small>Back to top</small></a>
		</p>
		<p>
			"Red foxes have rusty reddish-brown fur. This can vary in colouration and can give rise to black, silver or cross morphs. Their long, bushy tail, known as a 'brush' or 'sweep' is often tipped with white fur and the backs of the ears are black, as are part of the legs. They have white underparts.
			During the autumn and winter, the Red Fox will grow more fur. This so called 'winter fur' keeps the animal warm in colder environments. The fox sheds this fur at the beginning of spring, reverting back to the short fur for the duration of the summer.
			Although foxes are the smallest members of the dog family, the largest species of Red fox may reach an adult weight of 3 - 11 kilograms (6.5 - 24 pounds)."
		</p>
	</div>
    </body>
</html>

