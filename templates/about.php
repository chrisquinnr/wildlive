<html>
  <head>
    <script src='https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Sigmar+One' rel='stylesheet' type='text/css'>
    <link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.css' rel='stylesheet' />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <title>WildLive</title>
  </head>
  <body>
    <header>
      <h1>WildLive</h1>
      
      <nav>
        <ul>
          <li><a href="/">Home</a></li>
          <li><a href="/profile">My Profile</a></li>
          <li><a href="events">Events</a></li>
          <li><a href="/about">About</a></li>
          <li><a href="/help">Help</a></li>
        </ul>
      </nav>
    </header>
<div role="main">
	<h2>Spot it // Map it // Protect it</h2>
	
	<p>Building rich data profiles of our flora and fauna is one of the most valuable legacies we could leave future generations.</p> 
	
	<h2>Overview</h2>
	
	<p>A web app built on the <a href='http://www.slimframework.com/'>Slim PHP</a> framework and integrated with <a href='https://www.mapbox.com/mapbox.js/api/v2.1.5/'>Mapbox.js</a>.</p>
	
	<p>Designed to facilitate Citizen Science initiatives such as <a href='http://www.bnhc.org.uk/bioblitz/'>Bioblitz</a>, WildLive makes it super simple to record what wildlife you've seen.</p>
	
	<h2>Technical Description</h2>
	
	<p>Slim powers the app, with all the routing & most of the logic all in index.php.</p>
	
	<p>Login / authentication is handled by Facebook, and when logged in, users are presented with the <pre>wrapper.php</pre> template, which has been scaffolded solely for demo purposes. Actual user journey will be slightly different in finished app, but this view lets us show off a couple of features.</p>
	
	<p>A simple form is shown in the left sidebar, prepopulated with any & all infomation we can glean from Facebook, plus the lat/long coords obtained from the browser (after user permission has been granted).</p>
	
	<p>The map is the main visual component, displaying reported sightings of wildlife at their lat/long coords in real time.</p>
	
	<p>We store data in flat JSON encoded arrays, one sighting per file, handled by <a href='https://github.com/jamesmoss/flywheel'>Flywheel</a>.</p>
	
	
	
	<h2>Future developments</h2> 
	
	<ul>
		<li>More ways to login (Twitter / Google)</li>
		<li>Customised map styling</li>
		<li>Rolling the map "back in time" to see ebb & flow of reports</li>
		<li>Many more species icons</li>
		<li>Gamification / teams / leaderboards</li>
		<li>Help pages with information on flora & fauna</li>
		<li>Seasonal competitions</li>
		<li>Photo competitions</li>
	</ul>
</div>	
</body>
</html>
