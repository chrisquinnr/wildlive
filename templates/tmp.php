<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Archivo+Black">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <title></title>
  </head>
  <body>

    <header>



    </header>

    <div class="container">
      <div class="sixteen columns">
        <center>
          <div class="login-button">
            <fb:login-button scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button>
          </div>
          <div id="status"></div>
          <div class="submit-a-sighting-cta">
            <a class="button" href="/submit">Submit a sighting</a>
          </div>
        </center>
      </div>
    </div>

    <script src="/assets/vendor/jQuery/dist/jquery.min.js"></script>
    <script src="/assets/js/fb.js"></script>

  </body>
</html>
