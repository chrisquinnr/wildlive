<html>
  <head>
    <script src='https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Sigmar+One' rel='stylesheet' type='text/css'>
    <link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.css' rel='stylesheet' />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <title>WildLive</title>
    <script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "c3b2fc1b-5909-4d3f-af32-11abcd3b5da5", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
  </head>
  <body>
    <header>
      <h1>WildLive</h1>
      
      <nav>
        <ul>
          <li><a href="/">Home</a></li>
          <li><a href="/profile">My Profile</a></li>
          <li><a href="events">Events</a></li>
          <li><a href="/about">About</a></li>
          <li><a href="/help">Help</a></li>
        </ul>
      </nav>
    </header>
    
    <div role="main">
    	<h1>My Profile</h1>
    	<div class="row">
    	
    	</div>
    	<div class="row left">
    		<strong>Status</strong><br/>
    		Your ranking is <strong>Spotter!</strong>
    		<!-- Spotter (Green) - Plotter (Red) - Tracker (Purple) - Explorer (Bronze) -  Researcher (Silver) - Protector (Gold) --> 
    		<table>
	    		<tr>
	    			<td width=125 style='text-align: center'><img width=100 	src='/assets/images/ranks/spotter.png'></td>
	    			<td width=75 style='text-align: center'><img width=50 	src='/assets/images/ranks/plotter.png'></td>
	    			<td width=75 style='text-align: center'><img width=50 	src='/assets/images/ranks/tracker.png'></td>
	    			<td width=75 style='text-align: center'><img width=50 	src='/assets/images/ranks/explorer.png'></td>
	    			<td width=100 style='text-align: center'><img width=50 	src='/assets/images/ranks/researcher.png'></td>
	    			<td width=75 style='text-align: center'><img width=50 	src='/assets/images/ranks/protector.png'></td>
	    		</tr>
	    		<tr>
	    			<td style='text-align: center'><strong>Spotter</strong></td>
	    			<td style='text-align: center;color:grey;'>Plotter</td>
	    			<td style='text-align: center;color:grey;'>Tracker</td>
	    			<td style='text-align: center;color:grey;'>Explorer</td>
	    			<td style='text-align: center;color:grey;'>Researcher</td>
	    			<td style='text-align: center;color:grey;'>Protector</td>
	    		</tr>
    		</table>
    		
    		
    		
    		
    		
    		
    		
    	</div>
    	<hr/>
    	<div class="row">
    		<strong>Your spots:</strong>&nbsp;
    			<span style="font-size:200%">25</span> 
    			<br/>
    			<br/>
    	</div>
    	<hr/>
    	<div class="row">
    		<strong>Events you might be interested in</strong>
    	
    		<table width="500">
    			<tr style="border-top:1px dotted black">
    				<td width="20%">
	    				<img width="200" src='/assets/images/orgs/bnhc.png' style="float:left;padding:20;">
	    			</td>
	    			<td width="40%">
	    				<strong>BioBlitz // BS1 </strong><br/>
			    	</td>
			    	<td width="40%">
			    		<a class="button" href="/submit">Read more</a>
			    	</td>
			    </tr>
			    <tr style="border-top:1px dotted black">
    				<td width="20%">
	    				<img width="200" src='/assets/images/orgs/bbct.png' style="float:left;padding:20;">
	    			</td>
	    			<td width="40%">
	    				<strong>Bumblebee Conservation Trust </strong><br/>
			    	</td>
			    	<td width="40%">
			    		<a class="button" href="/submit">Read more</a>
			    	</td>
			    </tr>
			</table>
    	</div>
    	<hr/>
    	<div class="row">
    		<strong>Share your data!</strong><br/>
<span class='st_sharethis_large' displayText='ShareThis'></span>
<span class='st_facebook_large' displayText='Facebook'></span>
<span class='st_twitter_large' displayText='Tweet'></span>
<span class='st_linkedin_large' displayText='LinkedIn'></span>
<span class='st_pinterest_large' displayText='Pinterest'></span>
<span class='st_email_large' displayText='Email'></span>
    	</div>
    </div>
    
    
    </body>
    
</html>