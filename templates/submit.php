<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Archivo+Black">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <title></title>
  </head>
  <body>

    <div role="main">

        <form class="form" method="post" id="sighting" action="/submit" enctype="multipart/form-data">

          <input type="hidden" name="fbID" id="fbID" value="">

          <fieldset class="recorder">
            <legend>Spotter</legend>

            <div class="row">
              <label for="name">Name</label>
              <input id="name" name="name" type="text" value="">
            </div>

            <div class="row">
              <label for="email">Email</label>
              <input id="email" name="email" type="text" value="">
            </div>

            <div class="row">
              <label for="date">Date</label>
              <input id="date" name="date" type="text" value="">
            </div>

            <div class="row">
              <label for="latLang">Lat, Long</label>
              <input id="latLang" name="latLong" type="text" value="Loading ...">
            </div>

          </fieldset>
          <fieldset class="image">

            <legend>Image</legend>

            <div class="row">
              <!-- <label for="image">Upload an image</label> -->
              <input id="image" name="image" type="file" accept="image/*">
            </div>

          </fieldset>
          <fieldset class="animal">

            <legend>Species <a href="/help">?</a></legend>

            <div class="row">

                <div class="radio squirrel" data-type="squirrel"></div>
                <div class="radio fox" data-type="fox"></div>
                <div class="radio croc" data-type="croc"></div>
                <div class="radio swan" data-type="swan"></div>
                <div class="radio paw" data-type="paw"></div>
                <div class="radio butterfly" data-type="butterfly"></div>
                <div class="radio frog" data-type="frog"></div>
                <input type="hidden" name="species">

            </div>

            <div class="row">
              <label>Sex</label>
              <select id="occAttr:370" name="occAttr:370" class=" {required:true}">
                <option value=""></option>
                <option value="4683">present</option>
                <option value="4703">adult</option>
                <option value="4614">adult female</option>
                <option value="4615">adult male</option>
                <option value="4616">breeding (not sensitive record)</option>
                <option value="4617">breeding (sensitive record)</option>
                <option value="4618">Chick</option>
                <option value="4619">cocoon</option>
                <option value="4620">coppiced</option>
                <option value="4621">copulating pair</option>
                <option value="4622">dead (unknown sex/stage)</option>
                <option value="4623">dead adult</option>
                <option value="4624">dead adult female</option>
                <option value="4625">dead adult male</option>
                <option value="4626">dead female</option>
                <option value="4627">dead immature</option>
                <option value="4628">dead immature female</option>
                <option value="4629">dead immature male</option>
                <option value="4630">dead juvenile</option>
                <option value="4631">dead juvenile female</option>
                <option value="4632">dead juvenile male</option>
                <option value="4633">dead male</option>
                <option value="4634">eclipse</option>
                <option value="4635">eclipse female</option>
                <option value="4636">eclipse male</option>
                <option value="4637">eft</option>
                <option value="4638">egg</option>
                <option value="4639">egg mass</option>
                <option value="4640">exuvia</option>
                <option value="4641">female</option>
                <option value="4642">female exuvia</option>
                <option value="4643">gall</option>
                <option value="4644">gall causer</option>
                <option value="4645">hibernating</option>
                <option value="4646">hibernating female</option>
                <option value="4647">hibernating juvenile</option>
                <option value="4648">hibernating male</option>
                <option value="4649">hollow</option>
                <option value="4651">hollow or pollarded or veteran or very large tree</option>
                <option value="4650">hollow veteran or very large tree</option>
                <option value="4652">imago</option>
                <option value="4653">immature</option>
                <option value="4654">immature female</option>
                <option value="4655">immature male</option>
                <option value="4656">in bud</option>
                <option value="4657">in flower</option>
                <option value="4658">in fruit</option>
                <option value="4659">in seed</option>
                <option value="4660">juvenile</option>
                <option value="4661">juvenile female</option>
                <option value="4662">juvenile male</option>
                <option value="4663">larva</option>
                <option value="4664">larva female</option>
                <option value="4665">larva male</option>
                <option value="4666">male</option>
                <option value="4667">male exuvia</option>
                <option value="4668">mature</option>
                <option value="4669">mine</option>
                <option value="4670">negative record</option>
                <option value="4671">nestling</option>
                <option value="4672">nymph</option>
                <option value="4673">nymph female</option>
                <option value="4674">nymph male</option>
                <option value="4675">ovipositing</option>
                <option value="4676">ovipositing pair</option>
                <option value="4677">ovipositing single</option>
                <option value="4678">ovum</option>
                <option value="4679">pair</option>
                <option value="4680">parasitized</option>
                <option value="4681">pollarded</option>
                <option value="4682">pollarded veteran or very large tree</option>
                <option value="4684">pupa</option>
                <option value="4685">queen</option>
                <option value="4686">sapling</option>
                <option value="4687">seedling</option>
                <option value="4688">semi-mature</option>
                <option value="4689">shed skin (or slough)</option>
                <option value="4690">spawn</option>
                <option value="4691">standing dead</option>
                <option value="4692">summer plumage</option>
                <option value="4693">summer plumage female</option>
                <option value="4694">summer plumage male</option>
                <option value="4695">tadpole</option>
                <option value="4696">tandem pair</option>
                <option value="4697">teneral</option>
                <option value="4698">veteran or very large tree</option>
                <option value="4699">winter plumage</option>
                <option value="4700">winter plumage female</option>
                <option value="4701">winter plumage male</option>
                <option value="4702">worker</option>
              </select>
            </div>

            <div class="row">
              <label>Habitat</label>
              <select id="smpAttr:209" name="parent-smpAttr:209" class="hierarchy-select ">
                <option value=""></option>
                <option value="1716">Arable land, gardens or parks</option>
                <option value="1656">Bogs and fens</option>
                <option value="1640">Coast</option>
                <option value="1668">Grassland</option>
                <option value="1680">Heathland, scrub, hedgerow</option>
                <option value="1722">Industrial and urban</option>
                <option value="1648">Inland water</option>
                <option value="1758">Marine</option>
                <option value="1734">Mixed habitats</option>
                <option value="1704">Unvegetated or sparsely vegetated habitats</option>
                <option value="1692">Woodland</option>
              </select>

            </div>

          </fieldset>

          <div class="row">
            <button class="button">Submit</button>
          </div>

        </form>

      </div>

    <script src="/assets/vendor/jQuery/dist/jquery.min.js"></script>
    <script src="/assets/js/fb.js"></script>
    <script>
      (function($){

        var sighting = function(){
          var form = $('#sighting'),
              d = new Date()

          form.find('#date').val(d);

          navigator.geolocation.getCurrentPosition(function(data){
            var coords = data.coords;
            form.find('#latLang').val(coords.latitude + ', ' + coords.longitude);
          });

        }

        new sighting;

        $('.radio').on('click', function(){
          $('.radio').removeClass('selected');
          $(this).addClass('selected');
          $('[name=species]').val($(this).data('type'));
        })

        ////

        // window.fbAsyncInit = function() {
        //   FB.init({
        //     appId      : '399132593589074',
        //     xfbml      : true,
        //     version    : 'v2.2'
        //   });
        // };
        //
        // (function(d, s, id){
        //    var js, fjs = d.getElementsByTagName(s)[0];
        //    if (d.getElementById(id)) {return;}
        //    js = d.createElement(s); js.id = id;
        //    js.src = "//connect.facebook.net/en_US/sdk.js";
        //    fjs.parentNode.insertBefore(js, fjs);
        //  }(document, 'script', 'facebook-jssdk'));

      })(jQuery);
    </script>

  </body>
</html>
