# WildLive

## Spot it // Map it // Protect it

Building rich data profiles of our flora and fauna is one of the most valuable legacies we could leave future generations. 

### Overview

A web app built on the [Slim PHP](http://www.slimframework.com/) framework and integrated with [Mapbox.js](https://www.mapbox.com/mapbox.js/api/v2.1.5/).

Designed to facilitate Citizen Science initiatives such as [Bioblitz](http://www.bnhc.org.uk/bioblitz/), WildLive makes it super simple to record what wildlife you've seen.

### Technical Description

Slim powers the app, with all the routing & most of the logic all in index.php. 

Login / authentication is handled by Facebook, and when logged in, users are presented with the wrapper.php template, which has been scaffolded solely for demo purposes. Actual user journey will be slightly different in finished app, but this view lets us show off a couple of features.

A simple form is shown in the left sidebar, prepopulated with any & all infomation we can glean from Facebook, plus the lat/long coords obtained from the browser (after user permission has been granted).

The map is the main visual component, displaying reported sightings of wildlife at their lat/long coords in real time.

We store data in flat JSON encoded arrays, one sighting per file, handled by [Flywheel](https://github.com/jamesmoss/flywheel).



### Future developments 

* More ways to login (Twitter / Google)
* Customised map styling
* Rolling the map "back in time" to see ebb & flow of reports
* Many more species icons
* Gamification / teams / leaderboards
* Help pages with information on flora & fauna
* Seasonal competitions
* Photo competitions