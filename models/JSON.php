<?php

require 'vendor/autoload.php';

use JamesMoss\Flywheel;



class JSON {

	public $config;
	public $repo;

	public function __construct(){
		
		// @TODO Would make sense to check if folder is writable here
		$this->config = new \JamesMoss\Flywheel\Config('geojson/');
		$this->repo = new \JamesMoss\Flywheel\Repository('reports', $this->config);
	}


	public function buildGeoJSON($report){

				$species = ucfirst($report->species);

				$date    = date( 'd F Y H:i', strtotime($report->dateAdded) );
				$image   = 'http://'.$_SERVER['HTTP_HOST'].'/assets/uploads/'.$report->image;
				$content = "Spotted by <strong>".$report->name."</strong><br /> on ".$date;


				//$image = "http://featuredcreature.com/wp-content/uploads/2012/10/redfox2.jpg";
				$description = "<div class=\"tabs-ui\"><div class=\"tab\"><input type=\"radio\" id=\"text\" name=\"tab-group\" ><label for=\"text\">Description</label><div class=\"content\">".$content."</div></div><div class=\"tab\"><input type=\"radio\" id=\"image\" name=\"tab-group\" checked=\"true\"><label for=\"image\">Photo</label><div class=\"content\"><img src='".$image."' class=\"main-map--image\" alt=\"".$species."\" /></div></div></div>";

				//	$spotted = "<div class=\"tabs-ui\"><div class=\"tab\">Spotted by: " . $report->name . " on " . $report->dateAdded."</div></div>";
				
				
				// Species icon.png needs to match name of custom radio option 
				if(!$report->species){
					$iconurl = "/assets/images/logo.png";
				} else {
					$iconurl = "/assets/images/pins/".$report->species.".svg";
				}

				return array(
					"type" => "Feature",
					"geometry" => array(
						"type" => "Point",
						"coordinates" => array_reverse(explode(',',$report->latLong))
					),
					"properties" => array(
						"title" => $species,
						"description" => $description,
						"dateAdded" => $report->dateAdded,
						"icon" => array(
							"iconUrl" => $iconurl,
							"iconSize" => array(50, 50),
							"iconAnchor" => array(25, 25),
							"popupAnchor" => array(0, -25),
							"className" => "dot"
						)
					)
				);
	}

	public function writeReport($data){

		// Storing a new document
		
		$report = new \JamesMoss\Flywheel\Document(array(
				'name'					=> $data['name'],
				'email'					=> $data['email'],
				'dateAdded'				=> $data['date'],
				'species'     			=> $data['species'],
				'image'					=> $data['image'],
				'occAttr:370'			=> $data['occAttr:370'],
				'parent-smpAttr:209' 	=> $data['parent-smpAttr:209'],
				'fbID' 					=> $data['fbID'],
				'latLong'				=> $data['latLong']

		));

		$this->repo->store($report);
		return $report->id;
	}

	public function fetchAllReports(){
		return $this->repo->query()->execute();
	}

	public function fetchReport($id = null){
		
		if(!$id) return false; 
		
		$report = $this->repo->query()
		->where('id', '==', $id)
		->execute();

		return $report;
	}

	
	public function uploadFile($file){
		//var_dump($file);
		
		$upload_folder = dirname(__FILE__).'/../assets/uploads/';

		$temp = explode(".", $file["image"]["name"]);
		$extension = end($temp);
		
		$file_name = md5( time().$file['image']['name'] ).'.'.$extension;

		$target_file = $upload_folder.$file_name;

		move_uploaded_file($file["image"]["tmp_name"], $target_file);
		return $file_name;
		
	}
	
}

