<?php
require 'vendor/autoload.php';
require 'models/JSON.php';

$app = new \Slim\Slim();
$app->hook('slim.before', function () use ($app) {
    $app->view()->appendData(array('baseUrl' => '/assets/uploads'));
});
$app->get('/', function () use ($app) {

  $app->render('wrapper.php');

});


$app->get('/login', function () use ($app) {
  $app->render('login.php');
});
$app->get('/map', function ()  use ($app) {

	$app->render('map.php');
});
$app->get('/submit', function () use ($app) {
  $app->render('submit.php');
});
$app->get('/thanks', function () use ($app) {
  $app->render('thanks.php');
});
$app->post('/submit', function () use ($app) {
	
	$json = new JSON();
	$_POST['image'] = $json->uploadFile($_FILES);
	$id = $json->writeReport($_POST);
	//print_r($json->fetchReport($id));
	$app->redirect('/thanks');

});
$app->get('/help', function () use ($app) {
	$app->render('help.php');
});
$app->get('/about', function () use ($app) {
	$app->render('about.php');
});
$app->get('/profile', function () use ($app) {
	$app->render('profile.php');
});
$app->get('/events', function () use ($app) {
	$app->render('events.php');
});
		
$app->get('/api', function () {

  $json = new JSON();
  $reports = $json->fetchAllReports();
  $markers = [];
  foreach($reports as $report) $markers[] = $json->buildGeoJSON($report);

  header("Content-Type: application/json");

  echo json_encode($markers);

});
$app->run();
